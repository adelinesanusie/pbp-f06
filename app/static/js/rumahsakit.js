//Nama provinsi
$.ajax({
    // ajax url
    url: 'https://rs-bed-covid-api.vercel.app/api/get-provinces',
    dataType: 'json',
    type: 'GET',
    // Kalo datanya berhasil diambil, jalanin function success
    success: function(response) { // response itu data yang di return dari API
        const provinces = response.provinces;
        let selectBox = document.getElementById('format');
        for (let i = 0; i < provinces.length; i++ ) {
            let option = document.createElement('option');
            option.innerHTML = provinces[i].name;
            option.value = provinces[i].id;
            selectBox.appendChild(option);
        }
    }
});

//Nama kota
function getCity() {
    let selectBox = document.getElementById('format');
 var value = selectBox.value;
    $.ajax({
        url: `https://rs-bed-covid-api.vercel.app/api/get-cities?provinceid=${value}`,
        dataType: 'json',
        type: 'GET',
        success: function(response) {
            const cities = response.cities;
            let selectBox2 = document.getElementById('format2');
            selectBox2.innerHTML = "";
            for (let i=0; i<cities.length; i++) {
                let city = document.createElement('option');
                city.innerHTML = cities[i].name;
                city.value = `${value},${cities[i].id}`;
                selectBox2.appendChild(city);
            }
        }
    });
}

//Nama rs
function getHospital() {
    let value = document.getElementById('format2').value;
    let provinceId = value.split(',')[0];
    let cityId = value.split(',')[1];
    $.ajax({
        // ajax url
        url: `https://rs-bed-covid-api.vercel.app/api/get-hospitals?provinceid=${provinceId}&cityid=${cityId}&type=1`,
        dataType: 'json',
        type: 'GET',
        success: function(response) {
            const hospital = response.hospitals;
            hospital.forEach(function(hospital) {
                let card = `<div class="card" style="width: 18rem; align-items: center;">
                <div class="card-body">
                    <h5 class="card-title">${hospital.name}</h5>
                    <h6 class="card-subtitle mb-2 text-muted">${hospital.address}</h6>
                    <h6 class="card-subtitle mb-2 text-muted">${hospital.phone}</h6>
                    <h6 class="card-subtitle mb-2 text-muted">Kamar Tersedia: ${hospital.bed_availability}</h6>
                    <h6 class="card-subtitle mb-2 text-muted">${hospital.info}</h6>
                    </div>
                </div>`;
                $('#div').append(card);
            })
        }
    });
}