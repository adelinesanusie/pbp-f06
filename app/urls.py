from django.urls import path
from .views import index, login_request, logout_request, register_request, provinsi, rumahsakit, berita, forum

urlpatterns = [
    path('', index, name='index'),
    path('login/', login_request, name='login'),
    path('register/', register_request, name='register'),
    path('logout/', logout_request, name='logout'),
    path('provinsi/', provinsi, name='provinsi'),
    path('rumahsakit/', rumahsakit, name='rumahsakit'),
    path('berita/', berita, name='berita'),
    path('forum/', forum, name='forum'),
]